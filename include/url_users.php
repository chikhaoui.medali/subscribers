<?php

/*
Stores URLs that will need to include in users folder's files
*/

session_start();
$index_url='../index.php';
$login_url='login.php';
$register_url='register.php';
$logout_url='logout.php';
$profile_url='../users/profile.php';

include("../api/apiconnect.php");
include("../db/dbconnect.php");
include("../include/navbar.php");
include("../include/bootstrap_cdn.php");

?>
